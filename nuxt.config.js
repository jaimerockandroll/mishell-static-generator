const pkg = require('./package');


let config_params = require('./blog/config/configuration_parameters.json');


// glob is a small module to read 'globs', useful to get a filtered file list
const glob = require('glob');
// we acquire an array containing the filenames in the articles directory
let files = glob.sync( '**/*.md' , { cwd: 'blog/posts' });
files = files.concat(glob.sync( '**/*.md' , { cwd: 'blog/pages' }));

let page_routes = [];
for (let page=2; page <= Math.floor(files.length/2); page++) {
  page_routes.push('/page/'+page);
}
let routes = files.map(getSlugs).concat(page_routes);
console.log('routes to static gen', routes);
// We define a function to trim the '.md' from the filename and return the correct path.
function getSlugs(post, _) {
  let slug = post.substr(0, post.lastIndexOf('.'));
  return `/${slug}`;
}

module.exports = {
  mode: 'universal',
  router: {
    base: '/',// for base path
  },
  env: {
    blog_base_url: config_params["blog_base_url"],
    s_src_disqus: config_params["s_src_disqus"],
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lora|Adamina|Roboto+Slab:400,700|Material+Icons' },
      { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' }
    ],
    script: [
      // { src: '/header_scripts.js', defer:true}
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#900' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/material-kit.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/core-components.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /* You can extend webpack config here */
    /* this is importan for generate */
    extend(config, ctx) {
      const path = require('path');
      config.module.rules.push(
        {
          test: /\.md$/,
          include: path.resolve(__dirname, "blog/posts"),
          loader: "frontmatter-markdown-loader",
        },
        {
          test: /\.md$/,
          include: path.resolve(__dirname, "blog/pages"),
          loader: "frontmatter-markdown-loader",
        },
        {
          test: /\.html$/,
          include: path.resolve(__dirname, "blog/config"),
          loader: "html-loader",
        }
      );
    }
  },
  generate: {
    dir: 'public',
    extractCSS: true,
    routes: function() {
      return routes
    },
    html: {
      minify: {
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true
    }
  },
    /* this is importan for generate */
    extend(config, ctx) {
      const path = require('path');
      config.module.rules.push(
        {
          test: /\.md$/,
          include: path.resolve(__dirname, "blog/posts"),
          loader: "frontmatter-markdown-loader",
        },
        {
          test: /\.md$/,
          include: path.resolve(__dirname, "blog/pages"),
          loader: "frontmatter-markdown-loader",
        },
        {
          test: /\.html$/,
          include: path.resolve(__dirname, "blog/config"),
          loader: "html-loader",
        }
      );
    }
  }
};
