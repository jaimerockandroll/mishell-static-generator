---
title: 2. Viviendo una vida con sentido
date: 30-10-2029
image: https://i.postimg.cc/rmVz3Xs7/photo-1517800249805-f3d51bd0b07f-ixlib-rb-1-2.jpg
excerpt: Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos
---

It was during this time that a friend of mine contacted me saying he needed some front-end help finishing up a freelance project. I thought, 
<b>“This is a good opportunity for me to get used to working 100% remotely.”</b> I asked him if I could do it part-time and remotely from the evenings. He agreed.

In the evenings after my day job, I would go home and start working on the freelance project. During the days I spent working on this project I started realizing, “What if I could do my main job like this?”

Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos viviendo con intensidad Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos

<img src="https://i.postimg.cc/Y9djwnm0/photo-1469334031218-e382a71b716b-ixlib-rb-1-2.jpg
" style="max-width:100%">

Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos viviendo con intensidad Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos

### Blablabla

- Punto primero
- viviendo con intensidad 
- Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos
