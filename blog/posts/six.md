---
title: Ejemplo en html
date: 30-10-2032
image: https://i.postimg.cc/rmVz3Xs7/photo-1517800249805-f3d51bd0b07f-ixlib-rb-1-2.jpg
excerpt: Todos queremos vivir una vida con sentido, con pasión, sentir que cada día es algo que merece la pena y estamos
---

<h1 class="ql-align-center"> <span class="ql-font-serif"><span class="ql-cursor">﻿</span>I am Example 2!</span></span> </h1> <p><br></p> <p><span class="ql-font-serif">Whenever you play the game of thrones, you either win or die. There is no middle ground.</span></p> <p><br></p> <p><strong class="ql-font-serif">Some war against sword and spear to win, and the others the crow and the paper to win.</strong></p> <p><br></p> <p><u class="ql-font-serif">Dead history is write in ink, the living sort in blood.</u></p> <p><br></p> <p><em class="ql-font-serif">They're only numbers. Numbers on paper. Once you understand that, it's easy to make them behave.</em></p> <p><br></p> <p><span class="ql-font-serif">Every time we deal with an enemy, we create two more.</span></p> <p><br></p> <p><span class="ql-font-serif">So the king has decreed. The small council consents.</span></p> <p><br></p> <p><span class="ql-font-serif">Chaos not is a pit, chaos is a ladder.</span></p> <p><br></p> <p><span class="ql-font-serif">A chain needs all sorts of metals, and a land needs all sorts of people.</span></p> <p><br></p> <p><span class="ql-font-serif">When the snows fall and the white winds blow, the lone wolf dies, but the pack survives.</p>


