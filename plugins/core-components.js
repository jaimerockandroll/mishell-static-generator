import Vue from 'vue'
import Disqus from '~/components/Disqus.vue'
import Navbar from '~/components/MenuNavbar.vue'
import Myfooter from '~/components/Myfooter.vue'
import RelatedPosts from '~/components/RelatedPosts.vue'
import Subscription from '~/components/Subscription.vue'
import Post from '~/components/Post.vue'
import AtTheEndOfPost from '~/components/AtTheEndOfPost.vue'
import AtTheTopOfPost from '~/components/AtTheTopOfPost.vue'
import PostCard from '~/components/PostCard.vue'

Vue.component('AtTheTopOfPost',AtTheTopOfPost);
Vue.component('AtTheEndOfPost',AtTheEndOfPost);
Vue.component('Disqus',Disqus);
Vue.component('Navbar',Navbar);
Vue.component('Myfooter',Myfooter);
Vue.component('RelatedPosts',RelatedPosts);
Vue.component('Subscription',Subscription);
Vue.component('Post',Post);
Vue.component('PostCard',PostCard);
