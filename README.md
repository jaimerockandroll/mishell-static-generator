# mishell-static-generator

> My marvelous Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

## para imagenes:

https://postimg.cc/

## contact form:

https://usebasin.com


### Recursos a mirar para permisos git:
https://github.com/netlify/gotell
https://staticman.net

## Article reference:

We have followed this guide:

    https://regenrek.com/posts/create-a-frontmatter-markdown-powered-blog-with-nuxt-js-in-2019/
    
We have added frontmatter on nuxt.config.js:

      build: {
        /* You can extend webpack config here */
        extend(config, ctx) {
          const path = require('path')
          config.module.rules.push(
            {
              test: /\.md$/,
              include: path.resolve(__dirname, "posts"),
              loader: "frontmatter-markdown-loader",
            }
          );
        }
      },

We generate the /public static folder for easier deployment on gitlab pages (gitlab search for /public folder):
We use getSlugs function to get the posts:
(source: https://nirebu.com/blog/building-my-static-blog-with-nuxtjs-and-markdown-beginner)

      generate: {
        dir: 'public',
        routes: function() {
          return files.map(getSlugs)
          // return [
          //   '/hola',
          //   '/adios'
          // ]
        }
      }


Codepen para markdown:

    https://codepen.io/kvendrik/pen/Gmefv
